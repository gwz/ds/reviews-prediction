# Prediction - Amazon Fine Food Reviews

As a graduate student, I took my school's data science class, which consisted of 60, mostly Computer Science upperclassmen. One of the last assignments was to predict the number of stars (1 to 5) associated with each review in the Amazon Fine Food Reviews dataset as an in-class Kaggle competition. Without using deep learning and within the time constraints, I achieved an RMSE of [0.57, far better than any of my peers](https://www.kaggle.com/c/bu-cs506-fall2017/leaderboard), winning the competition.

* I used ensemble learning (random forest regressor)
* As features, I constructed valence scores from bigrams from the review text, review length and so on

[A two-page write-up of my approach](https://docs.google.com/document/d/1xDP_mJxwfrVfA2fm8wvBsaL2tg1oUL1IbIXLtrkPpCQ/edit)

Code is available upon request (not here in case the prompt is reused for other classes).
